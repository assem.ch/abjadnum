

##
# Counts letters occurrences and save it in self.results
#

class BaseCounter(object):

    def __init__(self):
        self.results = {}
        pass

    ##
    # Loads and analyzes an external XML document into this Counter.
    #
    # @param xml_source A file name or file object.  If a file object is
    #     given, it only has to implement a <b>read(n)</b> method.
    # @exception ParseError If the parser fails to parse the document.

    def run(self, xml_source):
        pass
