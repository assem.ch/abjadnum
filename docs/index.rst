.. Quran Stats documentation master file, created by
   sphinx-quickstart on Sat Sep 21 18:43:16 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Developer Guide
***************
 
.. toctree::
   :maxdepth: 2

   developer/index


.. Indices and tables
   ==================
   
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

