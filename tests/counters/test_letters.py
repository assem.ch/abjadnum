import unittest
from abjadnum.counters.letters import LettersCounter


class TestLettersCounter(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)

    def test_run(self):
        target = LettersCounter()
        target.run("")
        self.assertTrue(len(target.results) > 0)
