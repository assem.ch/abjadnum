Abjad: Science of Letters
'''''''''''''''''''''''''

**Warning:** This project is currently in an **alpha** state and currently not meant for real projects.

Introduction
************
 
This project is an attempt to study letters and words from the holy book.

Notable features:

* Frequencies count
* Abjad values from arabic text

Requirements 
************
 
Dependencies
============
 
* Python 2.7

Installation
************
 
Release
=======

::

   $ pip install abjadnum

Development
===========

::

   $ python setup.py dev
   $ python setup.py docs

Usage
*****

Console Script
==============

Compute letters or words frequencies
------------------------------------

::

 $ abjadnum-freqs (-l|-w) "text"
 $ abjadnum-freqs (-l|-w) [--tanzil-xml=path] surah:verse
 $ abjadnum-freqs (-l|-w) [--tanzil-xml=path] surah:verse end_surah:end_verse
 $ abjadnum-freqs (-l|-w) [--tanzil-xml=path] surah
 $ abjadnum-freqs (-l|-w) [--tanzil-xml=path] surah end_surah
 $ abjadnum-freqs (-l|-w) [--tanzil-xml=path] -f text_file

Options:

  -l, --letters
    Letters frequencies mode.

  -w, --words
    Words frequencies mode.

  -x, --tanzil-xml
    Quran text reference, see http://tanzil.net/download

Get Abdjad values
-----------------

::

 $ abjadnum [-l|-w|-v|-s] [-m=method] "text"
 $ abjadnum [-l|-w|-v|-s] [-m=method] [--tanzil-xml=path] surah:verse
 $ abjadnum [-l|-w|-v|-s] [-m=method] [--tanzil-xml=path] surah:verse end_surah:end_verse
 $ abjadnum [-l|-w|-v|-s] [-m=method] [--tanzil-xml=path] surah
 $ abjadnum [-l|-w|-v|-s] [-m=method] [--tanzil-xml=path] surah end_surah
 $ abjadnum [-l|-w|-v|-s] [-m=method] [--tanzil-xml=path] -f text_file

Options:

  -m, --method
    Algorithm to use, default: abjad
    Possible values: abjad, abjad-no-alif, ...

  -l, --letters
    Show details: letter level.

  -w, --words
    Show details: word level.

  -v, --verse
    Show details: verse level.

  -s, --surah
    Show details: surah level.

  -x, --tanzil-xml
    Quran text reference, see http://tanzil.net/download

Web Service
===========

#. Run webserver::

   $ abjadnum-svc ini_file

#. Run queries ::

     http://localhost:PORT/freq/(l|w)/surah:verse
     http://localhost:PORT/freq/(l|w)/surah:verse/end_surah:end_verse
     http://localhost:PORT/freq/(l|w)/surah
     http://localhost:PORT/freq/(l|w)/surah/end_surah
     http://localhost:PORT/freq/(l|w)/text

     http://localhost:PORT/(method)/(l|w|v|s|x)/surah:verse
     http://localhost:PORT/(method)/(l|w|v|s|x)/surah:verse/end_surah:end_verse
     http://localhost:PORT/(method)/(l|w|v|s|x)/surah
     http://localhost:PORT/(method)/(l|w|v|s|x)/surah/end_surah
     http://localhost:PORT/(method)/(l|w|v|s|x)/text

Web UI
======

#. Run webserver::

   $ abjadnum-webui ini_file

#. Launch Web UI::
     
     http://localhost:PORT/

Testing
*******

* Generic::

   $ python setup.py tests

* With Coverage::

   $ nosetests .


More Information 
****************
 
* Bitbucket : https://bitbucket.org/devaert/abjadnum
* Quran Text : http://tanzil.net/download
 
License 
*******
 
This project is licensed under the MIT license.

Support 
*******
 
* Issue Tracking : https://bitbucket.org/devaert/abjadnum/issues
* Pull Request : https://bitbucket.org/devaert/abjadnum/pull-requests

Those who wish to contribute directly to the project can contact me at dev.aert@gmail.com to talk about getting repository access granted.


