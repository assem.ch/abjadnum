import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.rst')) as f:
    CHANGES = f.read()

requires = [
    'distribute',
]

extras_requires = {
    'testing': ['nose', 'coverage'],
    'docs': ['sphinx'],
}

setup(name='abjadnum',
      version='0.0',
      description='Abjad Numerology',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
      ],
      author='',
      author_email='',
      url='',
      keywords='quran text stat',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      zip_safe=False,
      test_suite='tests',
      install_requires=requires,
      tests_require=extras_requires['testing'],
      extras_require=extras_requires,
      entry_points="""\
      [console_scripts]
      abjadnum-freq = abjadnum.scripts.cli_freqs:main
      """,
      )
